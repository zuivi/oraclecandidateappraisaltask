# Oracle candidate Appraisal Task

This project is a front end web application built using Oracle JET, the goal is to be able by the end to parse and visualize
payload issued by the backend ( e.g Jupyter like notebook)

### Getting started

```
git clone https://gitlab.com/zmazouzi/oraclecandidateappraisaltask
npm install
ojet serve
```


## Sample data

This an example of a valid input string

```
0	Series 1	Coke	3	28	59	8	12	17
1	Series 1	Pepsi	21	65	81	24	36	44
2	Series 1	Snapple	7	49	23	16	23	32
3	Series 1	Nestle	8	49	92	12	16	27
4	Series 2	Coke	12	35	46	17	21	24
5	Series 2	Pepsi	5	47	65	14	24	31
6	Series 2	Snapple	26	71	74	37	48	52
7	Series 2	Nestle	10	58	36	14	37	50

```

### Data validation and group/series selection process

If after parsing the input string a record has a length that is different from the first record than it is ignored (incomplete data set)
we assume that rows are structured data ( records with null values are omitted ).

For the selection of series/groups based on input data we count the number of distinct values for each column ( less distribution ) to use for series/groups
In a scatter plot and the quantitative values that have the most distribution would be select for x/y/z values
In a box plot and the quantitative values that have the most distribution would be used for low/q1/q2/q3/high and would follow the rule low < q1 < q2 < q3 < high

For example with the sample dataset it make more sense to have [Series 1, Series 2] as series than [17,44,32,27,24,24,31,52,50]


## License

This project is licensed under the MIT License

##
